from conans import ConanFile, tools


class EsiMaya22USBControlConan(ConanFile):
    name = "EsiMaya22USBControl"
    version = "0.5.0"
    license = "MIT"
    author = "Kai Dohmen psykai1993@googlemail.com"
    url = "https://gitlab.com/Psy-Kai/ESI-Maya22USBControl"
    description = "Dear Imgui based Gui application to control the ESI Maya22 "
    "USB external audio interface"
    settings = "os", "compiler", "build_type", "arch"
    exports_sources = "src/*"
    requires = [
        "glew/2.1.0",
        "glfw/3.3.2",
        "hidapi/0.9.0@psy-kai/stable",
        "imgui/1.77",
        "nlohmann_json/3.9.1",
        "standard_paths/0.1.1@psy-kai/stable"
    ]
    build_requires = [
        "Qbs/[>=1.17.0]@psy-kai/stable"
    ]
    _run_tests = tools.get_env("CONAN_RUN_TESTS", True)

    def build_requirements(self):
        self.build_requires("gtest/1.10.0", force_host_context=True);

    def build(self):
        runTests = ""
        if self._run_tests:
            runTests = "--all-products"
        self.run("qbs {} -f {} -d {} "
                 .format(runTests, self.source_folder,
                         self.build_folder),
                 run_environment=True)

    def package(self):
        self.copy("ESI-Maya22USB", keep_path=True)
