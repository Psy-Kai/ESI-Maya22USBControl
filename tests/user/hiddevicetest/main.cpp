#include <iostream>

#include <control.h>
#include <globals.h>
#include <hid.h>
#include <hiddevice.h>
#include <i2cwriter.h>
#include <outputchannel.h>

int main()
{
    esimaya22usb::Hid hid;
    try {
        hid_init();
        auto device = std::make_shared<esimaya22usb::HidDevice>(esimaya22usb::vendorId,
                                                                esimaya22usb::productId);

        esimaya22usb::I2cWriter i2cWriter(device);
        i2cWriter.write(esimaya22usb::commands::unmuteHeadphone, 0);
    } catch (const std::exception &e) {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
