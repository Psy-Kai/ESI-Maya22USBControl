import qbs

CppApplication {
    name: "Hid Device Usertest"

    files: [
        "main.cpp",
    ]

    Depends {
        name: "ESI-Maya22USB Library"
    }
}
