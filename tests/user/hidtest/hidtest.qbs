import qbs

CppApplication {
    name: "Hid Usertest"

    files: [
        "main.cpp",
    ]

    Depends {
        name: "ESI-Maya22USB Library"
    }
}
