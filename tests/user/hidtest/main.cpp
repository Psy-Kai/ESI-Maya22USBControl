#include <iostream>

#include <globals.h>
#include <hid.h>

std::wostream &operator<<(std::wostream &stream, const esimaya22usb::HidDeviceInfo &deviceInfo)
{
    std::wstring path{std::begin(deviceInfo.path), std::end(deviceInfo.path)};

    return stream << "\tPath: " << path << "\n"
                  << "\tVendor Id: " << std::hex << deviceInfo.vendorId << "\n"
                  << "\tProduct Id: " << deviceInfo.productId << "\n"
                  << "\tSerial Number: " << deviceInfo.serialNumber << "\n"
                  << "\tManufacturer: " << deviceInfo.manufacturerName << "\n"
                  << "\tProduct Name: " << deviceInfo.productName << "\n"
                  << "\tRelease Number: " << std::dec << deviceInfo.releaseNumber << "\n"
                  << "\tUsage: " << deviceInfo.usage << "\n"
                  << "\tUsage Page: " << deviceInfo.usagePage << "\n"
                  << "\tInterface Number: " << deviceInfo.interfaceNumber << "\n";
}

int main()
{
    try {
        esimaya22usb::Hid hid;
        hid.init();
        std::cout << "available devices:\n";
        const auto availableDevices =
            hid.availableDevices(esimaya22usb::vendorId, esimaya22usb::productId);
        int i = 0;
        for (const auto &device : availableDevices)
            std::wcout << "HID device info " << i++ << '\n' << device << '\n';
        hid.deinit();
    } catch (const std::exception &e) {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
