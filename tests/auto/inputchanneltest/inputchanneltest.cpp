#include <globals.h>
#include <i2cwritermock.h>
#include <inputchannel.h>

namespace esimaya22usb {
namespace test {

struct InputChannel : public testing::Test {
    std::unique_ptr<testing::StrictMock<I2cWriterMock>> i2cWriterMock =
        std::make_unique<testing::StrictMock<I2cWriterMock>>();
};

struct ChangeVolumeData final {
    uint8_t inputType;
    Volume::ValueType volumeLevel;
};

struct InputChannel_changeVolume :
    public InputChannel,
    public testing::WithParamInterface<ChangeVolumeData> {
    static std::vector<ChangeVolumeData> makeData()
    {
        std::vector<ChangeVolumeData> data;
        constexpr auto inputTypes =
            std::array{ControlInterface::Input::HiZ, ControlInterface::Input::Line,
                       ControlInterface::Input::Mic, ControlInterface::Input::MicHiZ};
        for (const auto inputType : inputTypes) {
            for (auto j = 0; j <= 100; ++j)
                data.emplace_back(ChangeVolumeData{static_cast<uint8_t>(inputType),
                                                   static_cast<Volume::ValueType>(j / 100.0)});
        }
        return data;
    }
};

INSTANTIATE_TEST_SUITE_P(, InputChannel_changeVolume,
                         testing::ValuesIn(InputChannel_changeVolume::makeData()));

TEST_P(InputChannel_changeVolume, left)
{
    const auto [inputType, volumeLevel] = GetParam();

    const Volume volume(volumeLevel);
    EXPECT_CALL(*i2cWriterMock, write(commands::selectInput, inputType)).Times(1);
    EXPECT_CALL(*i2cWriterMock,
                write(commands::volumeInLeft,
                      calcVolume(volumeLevel, levelbounds::inMin, levelbounds::inMax)))
        .Times(1);

    esimaya22usb::InputChannel channel(std::move(i2cWriterMock),
                                       static_cast<ControlInterface::Input>(inputType));
    channel.setVolumeLeft(volume);
}

TEST_P(InputChannel_changeVolume, right)
{
    const auto [inputType, volumeLevel] = GetParam();

    const Volume volume(volumeLevel);
    EXPECT_CALL(*i2cWriterMock, write(commands::selectInput, inputType)).Times(1);
    EXPECT_CALL(*i2cWriterMock,
                write(commands::volumeInRight,
                      calcVolume(volumeLevel, levelbounds::inMin, levelbounds::inMax)))
        .Times(1);

    esimaya22usb::InputChannel channel(std::move(i2cWriterMock),
                                       static_cast<ControlInterface::Input>(inputType));
    channel.setVolumeRight(volume);
}

} // namespace test
} // namespace esimaya22usb

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
