#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <channelfactorymock.h>
#include <channelmock.h>
#include <control.h>
#include <globals.h>
#include <hiddevicemock.h>
#include <i2cwriter.h>
#include <inputchannel.h>

namespace esimaya22usb {
namespace test {
constexpr auto dataCount = 32;
namespace stub {
class Channel final : public ChannelInterface {
public:
    Volume volumeLeft() const noexcept override
    {
        return {1.0};
    }
    void setVolumeLeft(Volume) override {}
    Volume volumeRight() const noexcept override
    {
        return {1.0};
    }
    void setVolumeRight(Volume) override {}
    State state() const noexcept override
    {
        return {};
    }
    void setState(State) override {}
};
} // namespace stub

struct Control : public testing::Test {
    std::unique_ptr<testing::StrictMock<ChannelFactoryMock>> channelFactoryMock =
        std::make_unique<testing::StrictMock<ChannelFactoryMock>>();
    std::shared_ptr<testing::StrictMock<HidDeviceMock>> hidDeviceMock =
        std::make_shared<testing::StrictMock<HidDeviceMock>>();
    testing::StrictMock<ChannelFactoryMock> *channelFactoryMockRaw = channelFactoryMock.get();
    stub::Channel *outputChannel = nullptr;
    stub::Channel *inputChannel = nullptr;
    std::unique_ptr<esimaya22usb::Control> control;

    void testCreateControl()
    {
        EXPECT_CALL(*hidDeviceMock, write(testing::_)).WillOnce(testing::Return(dataCount));
        outputChannel = new stub::Channel{};
        inputChannel = new stub::Channel{};
        EXPECT_CALL(*channelFactoryMockRaw, createChannel(esimaya22usb::Control::Output::Standard))
            .WillOnce(
                testing::Return(testing::ByMove(std::unique_ptr<ChannelInterface>(outputChannel))));
        EXPECT_CALL(*channelFactoryMockRaw, createChannel(esimaya22usb::Control::Input::Mic))
            .WillOnce(
                testing::Return(testing::ByMove(std::unique_ptr<ChannelInterface>(inputChannel))));
        control =
            std::make_unique<esimaya22usb::Control>(hidDeviceMock, std::move(channelFactoryMock));
        ASSERT_EQ(&control->outputChannel(), outputChannel);
        ASSERT_EQ(&control->inputChannel(), inputChannel);
    }
};

struct Control_setInput :
    public Control,
    public testing::WithParamInterface<esimaya22usb::Control::Input> {};

INSTANTIATE_TEST_SUITE_P(, Control_setInput,
                         testing::Values(esimaya22usb::Control::Input::Mic,
                                         esimaya22usb::Control::Input::HiZ,
                                         esimaya22usb::Control::Input::Line,
                                         esimaya22usb::Control::Input::MicHiZ));

TEST_P(Control_setInput, setInput)
{
    testCreateControl();
    if (testing::Test::HasFatalFailure())
        return;

    auto channel = std::make_unique<testing::StrictMock<ChannelMock>>();
    auto *channelRaw = channel.get();
    const auto &castedInput = static_cast<ControlInterface::Input>(GetParam());

    EXPECT_CALL(*channelFactoryMockRaw, createChannel(castedInput))
        .WillOnce(testing::Return(testing::ByMove(std::move(channel))));

    control->setInput(castedInput);

    ASSERT_EQ(&control->inputChannel(), channelRaw);
}

TEST_F(Control, changeMonitor)
{
    testCreateControl();
    if (testing::Test::HasFatalFailure())
        return;
    EXPECT_CALL(*hidDeviceMock, write).WillOnce(testing::Return(dataCount));
    control->setMonitor(ControlInterface::Monitoring::Enabled);
    EXPECT_CALL(*hidDeviceMock, write).WillOnce(testing::Return(dataCount));
    control->setMonitor(ControlInterface::Monitoring::Disabled);
}

} // namespace test
} // namespace esimaya22usb

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
