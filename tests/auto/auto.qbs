import qbs

Project {
    name: "Auto"

    references: [
        "controltest",
        "i2cwritertest",
        "inputchanneltest",
        "outputchanneltest",
    ]

    AutotestRunner {}
}
