#include <globals.h>
#include <i2cwritermock.h>
#include <outputchannel.h>

namespace esimaya22usb {
namespace test {

struct OutputChannel : public testing::Test {};

struct OutputChannel_changeVolume :
    public OutputChannel,
    public testing::WithParamInterface<Volume::ValueType> {};

INSTANTIATE_TEST_SUITE_P(, OutputChannel_changeVolume, testing::Range(0.0f, 1.0f, 1 / 100.0f));

TEST_P(OutputChannel_changeVolume, left)
{
    auto i2cWriterMock = std::make_unique<I2cWriterMock>();
    const auto volumeLevel = GetParam();

    EXPECT_CALL(*i2cWriterMock,
                write(commands::volumeOutLeft,
                      calcVolume(volumeLevel, levelbounds::outMin, levelbounds::outMax)))
        .Times(1);

    esimaya22usb::OutputChannel channel(std::move(i2cWriterMock));
    channel.setVolumeLeft(Volume(volumeLevel));
}

TEST_P(OutputChannel_changeVolume, right)
{
    auto i2cWriterMock = std::make_unique<I2cWriterMock>();
    const auto volumeLevel = GetParam();

    EXPECT_CALL(*i2cWriterMock,
                write(commands::volumeOutRight,
                      calcVolume(volumeLevel, levelbounds::outMin, levelbounds::outMax)))
        .Times(1);

    esimaya22usb::OutputChannel channel(std::move(i2cWriterMock));
    channel.setVolumeRight(Volume(volumeLevel));
}

} // namespace test
} // namespace esimaya22usb

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
