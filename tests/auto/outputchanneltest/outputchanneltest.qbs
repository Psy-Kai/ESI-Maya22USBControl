import qbs

CppApplication {
    name: "Outputchannel Test"
    condition: qbs.debugInformation && gtest.present
    type: base.concat("autotest")

    files: [
        "outputchanneltest.cpp",
    ]

    Properties {
        condition: !qbs.targetOS.contains("windows")
        cpp.driverFlags: base.concat("-pthread")
    }

    Depends {
        name: "gtest"
        required: false
    }
    Depends {
        name: "ESI-Maya22USB Library"
    }
}
