#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <hiddevicemock.h>
#include <i2cwriter.h>

namespace esimaya22usb {
namespace test {
namespace i2cwriter {

struct Data final {
    uint8_t command;
    uint8_t data;
    bool result;
};

class I2cWriter : public testing::TestWithParam<Data> {};

std::vector<Data> generateData()
{
    auto data = std::vector<Data>{};
    for (uint8_t i = 0; i < 25; ++i) {
        for (uint8_t j = 0; j < 25; ++j)
            data.emplace_back(Data{static_cast<uint8_t>(std::rand() % 256),
                                   static_cast<uint8_t>(std::rand() % 256),
                                   static_cast<bool>(std::rand() % 2)});
    }
    return data;
}

INSTANTIATE_TEST_SUITE_P(, I2cWriter, testing::ValuesIn(generateData()));

TEST_P(I2cWriter, i2cWriter)
{
    auto hidDeviceMock = std::make_shared<testing::StrictMock<HidDeviceMock>>();
    const auto data = GetParam();

    HidDeviceMock::Buffer arg(32, 0);
    arg[1] = 0x12;
    arg[2] = 0x34;
    arg[3] = data.command;
    arg[5] = 1;
    arg[6] = data.data;
    arg[22] = 0x80;
    const uint8_t wrongResult = 11;
    EXPECT_CALL(*hidDeviceMock, write(arg))
        .WillOnce(::testing::Return(data.result ? arg.size() : wrongResult));

    esimaya22usb::I2cWriter i2cWriter(hidDeviceMock);
    bool exception = false;
    try {
        i2cWriter.write(data.command, data.data);
    } catch (const std::exception &) {
        exception = true;
    }

    if (data.result)
        ASSERT_FALSE(exception);
    else
        ASSERT_TRUE(exception);
}

} // namespace i2cwriter
} // namespace test
} // namespace esimaya22usb

int main(int argc, char *argv[])
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
