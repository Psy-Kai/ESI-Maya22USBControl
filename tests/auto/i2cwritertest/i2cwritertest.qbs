import qbs

CppApplication {
    name: "I2C Writer Test"
    condition: qbs.debugInformation && gtest.present
    type: base.concat("autotest")

    files: [
        "i2cwritertest.cpp",
    ]

    Properties {
        condition: !qbs.targetOS.contains("windows")
        cpp.driverFlags: base.concat("-pthread")
    }

    Depends {
        name: "gtest"
        required: false
    }
    Depends {
        name: "ESI-Maya22USB Library"
    }
}
