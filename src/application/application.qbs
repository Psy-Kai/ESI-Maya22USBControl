import qbs

CppApplication {
    name: "ESI-Maya22USB"

    files: [
        "main.cpp",
    ]

    cpp.cxxLanguageVersion: "c++17"

    Properties {
        condition: !qbs.targetOS.contains("windows")
        cpp.driverFlags: base.concat("-pthread")
    }

    Group {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
    }

    Depends {
        name: "ConanBasicSetup"
    }
    Depends {
        name: "ESI-Maya22USB Library"
    }
}
