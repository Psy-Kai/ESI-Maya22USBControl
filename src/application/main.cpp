#include <iostream>

#include <standard_paths/standard_paths.h>

#include <channelfactory.h>
#include <control.h>
#include <globals.h>
#include <hid.h>
#include <hiddevice.h>
#include <settings.h>
#include <ui/mainwindow.h>

int main(int argc, char *argv[])
{
    assert(1 <= argc);
    auto app = standard_paths::App{standard_paths::Filesystem::path{argv[0]}.filename().string()};

    try {
        esimaya22usb::Hid hid;
        hid.init();

        auto hidDevice = std::make_shared<esimaya22usb::HidDevice>(esimaya22usb::vendorId,
                                                                   esimaya22usb::productId);

        try {
            esimaya22usb::Control control{
                hidDevice, std::make_unique<esimaya22usb::ChannelFactory>(hidDevice)};
            {
                esimaya22usb::Settings settings;
                settings.load(control);
            }

            esimaya22usb::ui::MainWindow window{control};
            while (!window.shouldClose())
                window.render();

            {
                esimaya22usb::Settings settings;
                settings.save(control);
            }
        } catch (const esimaya22usb::HidException &e) {
            std::wcerr << e.what() << ": " << hidDevice->errorString() << '\n';
            return EXIT_FAILURE;
        } catch (const std::exception &e) {
            std::cerr << e.what() << '\n';
            return EXIT_FAILURE;
        }

        hid.deinit();
    } catch (const std::exception &e) {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
