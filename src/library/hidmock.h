#pragma once

#include <gmock/gmock.h>

#include "hidinterface.h"

namespace esimaya22usb {
class HidMock : public HidInterface {
public:
    MOCK_METHOD(void, init, (), (const, override));
    MOCK_METHOD(void, deinit, (), (const, override));
    MOCK_METHOD(std::vector<HidDeviceInfo>, availableDevices,
                (uint16_t vendorId, uint16_t productId), (const, override));
};
} // namespace esimaya22usb
