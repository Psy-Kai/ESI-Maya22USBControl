#pragma once

#include <memory>

#include "controlinterface.h"

namespace esimaya22usb {

class ChannelFactoryInterface {
public:
    virtual ~ChannelFactoryInterface() = default;
    virtual std::unique_ptr<ChannelInterface>
        createChannel(ControlInterface::Input) const noexcept = 0;
    virtual std::unique_ptr<ChannelInterface>
        createChannel(ControlInterface::Output) const noexcept = 0;
};

} // namespace esimaya22usb
