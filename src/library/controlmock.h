#pragma once

#include <gmock/gmock.h>

#include "controlinterface.h"

namespace esimaya22usb {
class ControlMock : public ControlInterface {
public:
    MOCK_METHOD(Input, input, (), (const, noexcept, override));
    MOCK_METHOD(void, setInput, (Input), (override));
    MOCK_METHOD(Monitoring, monitor, (), (const, noexcept, override));
    MOCK_METHOD(void, setMonitor, (Monitoring), (override));
    MOCK_METHOD(ChannelInterface &, inputChannel, (), (const, noexcept, override));
    MOCK_METHOD(ChannelInterface &, outputChannel, (), (const, noexcept, override));
};
} // namespace esimaya22usb
