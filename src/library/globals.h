#pragma once

#include <cmath>
#include <cstdint>

namespace esimaya22usb {
/* can be queried with lsusb */
constexpr uint16_t vendorId = 0x2573;
constexpr uint16_t productId = 0x0017;

namespace levelbounds {
constexpr uint8_t outMin = 110;
constexpr uint8_t outMax = 110 + 145;
constexpr uint8_t inMin = 104;
constexpr uint8_t inMax = 104 + 127;
} // namespace levelbounds

namespace commands {
constexpr uint8_t unmuteHeadphone = 0x0d << 1;
constexpr uint8_t volumeOutLeft = (0x03 << 1) + 1;
constexpr uint8_t volumeOutRight = (0x04 << 1) + 1;
constexpr uint8_t volumeInLeft = 0x0e << 1;
constexpr uint8_t volumeInRight = 0x0f << 1;
constexpr uint8_t selectInput = 0x15 << 1;
constexpr uint8_t muteMask = 0xc0;
constexpr uint8_t monitoring = 0x16 << 1;
} // namespace commands

inline uint8_t calcVolume(const float level, const uint8_t min, const uint8_t max)
{
    uint8_t maxOut = max - min;
    uint8_t vol = std::lroundf(level * maxOut);
    return vol + min;
}

} // namespace esimaya22usb
