#include "hiddevice.h"

#include <stdexcept>

namespace esimaya22usb {

constexpr auto stringBufferSize = 255;
constexpr auto dataBufferSize = 2046;

HidDevice::HidDevice(uint16_t vendorId, uint16_t productId,
                     const HidDeviceInterface::Text &serialNumber) :
    m_hidDevice{open(vendorId, productId, serialNumber), Deleter{this}}
{}

HidDevice::~HidDevice() = default;

int32_t HidDevice::write(const HidDeviceInterface::Buffer &data)
{
    const auto writeCount = hid_write(m_hidDevice.get(), data.data(), data.size());
    if (writeCount < 0)
        throw HidException{"write failed"};
    return writeCount;
}

HidDeviceInterface::Buffer
HidDevice::readTimeout(const std::chrono::milliseconds milliseconds) const
{
    Buffer buffer(dataBufferSize, 0);
    const auto readCount =
        hid_read_timeout(m_hidDevice.get(), buffer.data(), buffer.size(), milliseconds.count());
    if (readCount < 0)
        throw HidException{"read failed"};
    return buffer;
}

HidDeviceInterface::Buffer HidDevice::read() const
{
    Buffer buffer(dataBufferSize, 0);
    const auto readCount = hid_read(m_hidDevice.get(), buffer.data(), buffer.size());
    if (readCount < 0)
        throw HidException{"read failed"};
    return buffer;
}

int32_t HidDevice::sendFeatureReport(const Buffer &featureReport)
{
    const auto writeCount =
        hid_send_feature_report(m_hidDevice.get(), featureReport.data(), featureReport.size());
    if (writeCount < 0)
        throw HidException{"Failed to send feature report"};
    return writeCount;
}

HidDeviceInterface::Buffer HidDevice::queryFeatureReport() const
{
    std::basic_string<uint8_t> featureReport(dataBufferSize, 0);
    const auto readCount =
        hid_get_feature_report(m_hidDevice.get(), featureReport.data(), featureReport.size());
    if (readCount < 0)
        throw HidException{"Failed to query feature report"};
    featureReport.resize(readCount);
    return featureReport;
}

HidDeviceInterface::Text HidDevice::queryManufacturer() const
{
    Text manufacturer(stringBufferSize, 0);
    if (hid_get_manufacturer_string(m_hidDevice.get(), manufacturer.data(), stringBufferSize))
        throw HidException{"Failed to query manufacturer"};
    return manufacturer;
}

HidDeviceInterface::Text HidDevice::queryProduct() const
{
    Text product(stringBufferSize, 0);
    if (hid_get_product_string(m_hidDevice.get(), product.data(), stringBufferSize))
        throw HidException{"Failed to query product"};
    return product;
}

HidDeviceInterface::Text HidDevice::querySerialNumber() const
{
    Text serialNumber(stringBufferSize, 0);

    if (hid_get_serial_number_string(m_hidDevice.get(), serialNumber.data(),
                                     serialNumber.capacity()))
        throw HidException{"Failed to query serial number"};
    return serialNumber;
}

HidDeviceInterface::Text HidDevice::errorString() const noexcept
{
    return hid_error(m_hidDevice.get());
}

hid_device *HidDevice::open(uint16_t vendorId, uint16_t productId,
                            const HidDeviceInterface::Text &serialNumber) const
{
    hid_device *device = nullptr;

    if (serialNumber.empty())
        device = hid_open(vendorId, productId, nullptr);
    else
        device = hid_open(vendorId, productId, serialNumber.data());

    if (device == nullptr)
        throw HidException{"Failed to open device"};
    return device;
}

hid_device *HidDevice::open(const std::string &path) const
{
    auto *device = hid_open_path(path.data());
    if (device == nullptr)
        throw HidException{"Failed to open device at " + path};
    return device;
}

hid_device *HidDevice::open(const HidDeviceInfo &deviceInfo) const
{
    return open(deviceInfo.vendorId, deviceInfo.productId, deviceInfo.serialNumber);
}

void HidDevice::close(hid_device *device) const
{
    hid_close(device);
}

} // namespace esimaya22usb
