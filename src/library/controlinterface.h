#pragma once

#include "channelinterface.h"

namespace esimaya22usb {

class ControlInterface {
public:
    enum class Input { Mic = 1, HiZ = 2, Line = 4, MicHiZ = 8 };
    enum class Output { Standard = 0 };
    enum class Monitoring { Disabled = 1, Enabled = 5 };
    virtual ~ControlInterface() = default;
    virtual Input input() const noexcept = 0;
    virtual void setInput(Input) = 0;
    virtual Monitoring monitor() const noexcept = 0;
    virtual void setMonitor(Monitoring) = 0;
    virtual ChannelInterface &inputChannel() const noexcept = 0;
    virtual ChannelInterface &outputChannel() const noexcept = 0;
};

} // namespace esimaya22usb
