#pragma once

#include <gmock/gmock.h>

#include "i2cwriterinterface.h"

namespace esimaya22usb {
class I2cWriterMock : public I2cWriterInterface {
public:
    MOCK_METHOD(void, write, (const uint8_t command, const uint8_t data), (override));
};
} // namespace esimaya22usb
