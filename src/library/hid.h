#pragma once

#include "hidinterface.h"

namespace esimaya22usb {

class Hid final : public HidInterface {
public:
    void init() const override;
    void deinit() const override;
    std::vector<HidDeviceInfo> availableDevices(uint16_t vendorId,
                                                uint16_t productId) const override;
};

} // namespace esimaya22usb
