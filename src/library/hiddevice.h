#pragma once

#include <memory>

#include "hiddeviceinterface.h"

namespace esimaya22usb {

class HidDevice final : public HidDeviceInterface {
public:
    HidDevice(uint16_t vendorId, uint16_t productId, const Text &serialNumber = {});
    explicit HidDevice(const std::string &path);
    explicit HidDevice(const HidDeviceInfo &deviceInfo);
    ~HidDevice() override;
    int32_t write(const Buffer &data) override;
    Buffer readTimeout(const std::chrono::milliseconds milliseconds) const override;
    Buffer read() const override;
    int32_t sendFeatureReport(const Buffer &featureReport) override;
    Buffer queryFeatureReport() const override;
    Text queryManufacturer() const override;
    Text queryProduct() const override;
    Text querySerialNumber() const override;
    Text errorString() const noexcept override;

private:
    struct Deleter final {
        Deleter(HidDevice *closer) noexcept : closer{closer} {}
        void operator()(hid_device *device) const
        {
            closer->close(device);
        }
        HidDevice *closer = nullptr;
    };

    hid_device *open(uint16_t vendorId, uint16_t productId,
                     const Text &serialNumber = {}) const override;
    hid_device *open(const std::string &path) const override;
    hid_device *open(const HidDeviceInfo &deviceInfo) const override;
    void close(hid_device *device) const override;

    std::unique_ptr<hid_device, Deleter> m_hidDevice;
};

} // namespace esimaya22usb
