#pragma once

#include <memory>

#include "channelinterface.h"
#include "controlinterface.h"
#include "i2cwriterinterface.h"

namespace esimaya22usb {

class InputChannel final : public ChannelInterface {
public:
    explicit InputChannel(std::unique_ptr<I2cWriterInterface> i2cWriter,
                          ControlInterface::Input inputType);
    Volume volumeLeft() const noexcept override;
    void setVolumeLeft(Volume) override;
    Volume volumeRight() const noexcept override;
    void setVolumeRight(Volume) override;
    State state() const noexcept override;
    void setState(State) override;

private:
    const std::unique_ptr<I2cWriterInterface> m_i2cWriter;
    const ControlInterface::Input m_inputType;
    Volume m_volumeLeft = 1.0f;
    Volume m_volumeRight = 1.0f;
    State m_state;
};
} // namespace esimaya22usb
