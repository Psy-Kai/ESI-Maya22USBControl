#include "volume.h"

#include <stdexcept>

namespace esimaya22usb {

Volume::Volume(Volume::ValueType level)
{
    *this = level;
}

bool Volume::operator==(Volume &other) const noexcept
{
    return m_volumeLevel == other.m_volumeLevel;
}

Volume &Volume::operator=(const ValueType volumeLevel)
{
    if (volumeLevel < 0)
        throw std::runtime_error{"Volume too low"};
    if (1 < volumeLevel)
        throw std::runtime_error{"Volume too high"};
    m_volumeLevel = volumeLevel;
    return *this;
}

Volume::operator ValueType() const
{
    return m_volumeLevel;
}

} // namespace esimaya22usb
