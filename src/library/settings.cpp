#include "settings.h"

#include <iostream>
#include <standard_paths/standard_paths.h>

#include "channelinterface.h"

namespace esimaya22usb {

namespace key {
constexpr auto selectedInput = std::string_view{"selectedInput"};
constexpr auto input = std::string_view{"input"};
constexpr auto output = std::string_view{"output"};
constexpr auto left = std::string_view{"left"};
constexpr auto right = std::string_view{"right"};
constexpr auto state = std::string_view{"state"};
} // namespace key

Settings::Settings() = default;

void Settings::save(const ControlInterface &control)
{
    nlohmann::json settings;

    saveControl(settings, control);
    saveChannel(settings[key::input.data()], control.inputChannel());
    saveChannel(settings[key::output.data()], control.outputChannel());

    std::clog << "save configuration to " << standard_paths::appConfig() << ": " << settings
              << '\n';

    auto stream = std::ofstream{standard_paths::appConfig()};
    stream << settings;
}

void Settings::load(ControlInterface &control)
{
    try {
        nlohmann::json settings;
        auto stream = std::ifstream{standard_paths::appConfig()};
        stream >> settings;
        loadControl(settings, control);
        loadChannel(settings[key::input.data()], control.inputChannel());
        loadChannel(settings[key::output.data()], control.outputChannel());
    } catch (const std::exception &e) {
        std::cerr << "failed to load configuration from " << standard_paths::appConfig() << ": "
                  << e.what() << '\n';
    }
}

void Settings::saveControl(nlohmann::json &settings, const ControlInterface &control)
{
    using Type = std::underlying_type_t<ControlInterface::Input>;
    settings[key::selectedInput.data()] = static_cast<Type>(control.input());
}

void Settings::saveChannel(nlohmann::json &settings, const ChannelInterface &channel)
{
    settings[key::left.data()] = channel.volumeLeft().operator float();
    settings[key::right.data()] = channel.volumeRight().operator float();
    using Type = std::underlying_type_t<ChannelInterface::State>;
    settings[key::state.data()] = static_cast<Type>(channel.state());
}

void Settings::loadControl(const nlohmann::json &settings, ControlInterface &control)
{
    using Type = std::underlying_type_t<ControlInterface::Input>;
    control.setInput(
        static_cast<ControlInterface::Input>(settings[key::selectedInput.data()].get<Type>()));
}

void Settings::loadChannel(const nlohmann::json &settings, ChannelInterface &channel)
{
    channel.setVolumeLeft(settings[key::left.data()].get<Volume::ValueType>());
    channel.setVolumeRight(settings[key::right.data()].get<Volume::ValueType>());
    using Type = std::underlying_type_t<ChannelInterface::State>;
    channel.setState(static_cast<ChannelInterface::State>(settings[key::state.data()].get<Type>()));
}

} // namespace esimaya22usb
