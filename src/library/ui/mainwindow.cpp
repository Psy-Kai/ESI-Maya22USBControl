#include "mainwindow.h"

#include <stdexcept>

#include <bindings/imgui_impl_glfw.h>
#include <bindings/imgui_impl_opengl3.h>

#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h> // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h> // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h> // Initialize with gladLoadGL()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD2)
#include <glad/gl.h> // Initialize with gladLoadGL(...) or gladLoaderLoadGL()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING2)
#define GLFW_INCLUDE_NONE // GLFW including OpenGL headers causes ambiguity or multiple definition
                          // errors.
#include <glbinding/Binding.h> // Initialize with glbinding::Binding::initialize()
#include <glbinding/gl/gl.h>
using namespace gl;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING3)
#define GLFW_INCLUDE_NONE // GLFW including OpenGL headers causes ambiguity or multiple definition
                          // errors.
#include <glbinding/gl/gl.h>
#include <glbinding/glbinding.h> // Initialize with glbinding::initialize()
using namespace gl;
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif

#include <GLFW/glfw3.h>

#include "customimgui.h"

namespace esimaya22usb {
namespace ui {
namespace key {
constexpr auto labelSelectInput = std::string_view{"Select Input"};
constexpr auto windowSelectInput = std::string_view{"Select Input box"};
namespace windowselectinput {
constexpr auto width = 120.0f;
constexpr auto buttonLineIn = std::string_view{"Line In"};
constexpr auto buttonMic = std::string_view{"Mic"};
constexpr auto buttonHiZ = std::string_view{"Hi-Z"};
constexpr auto buttonMicHiZ = std::string_view{"Mic Hi-Z"};
constexpr auto labelMonitoring = std::string_view{"Monitoring"};
namespace buttonmonitoring {
constexpr auto enabled = std::string_view{"enabled"};
constexpr auto disabled = std::string_view{"disabled"};
} // namespace buttonmonitoring
} // namespace windowselectinput
constexpr auto windowChannels = std::string_view("Channels");
namespace windowchannels {
constexpr auto input = std::string_view{"Input"};
constexpr auto output = std::string_view{"Output"};
} // namespace windowchannels
} // namespace key

void glfwErrorCallback(int error, const char *description)
{
    std::cerr << "glfw error: " << error << " | " << description << '\n';
}

MainWindow::MainWindow(ControlInterface &control) :
    m_control{control}, m_window{nullptr, &glfwDestroyWindow}
{
    glfwSetErrorCallback(glfwErrorCallback);
    if (!glfwInit())
        throw std::runtime_error{"Failed to initialize glfw"};

    m_window.reset(glfwCreateWindow(800, 600, "Imgui Prototype", nullptr, nullptr));
    if (m_window == nullptr)
        throw std::runtime_error{"Could not create glfw window"};
    glfwMakeContextCurrent(m_window.get());
    glfwSwapInterval(1);
    if (!initializeOpenGlLoader())
        throw std::runtime_error{"Failed to initialize OpenGL loader"};

    ImGui::CreateContext();

    ImGui_ImplGlfw_InitForOpenGL(m_window.get(), true);
    ImGui_ImplOpenGL3_Init();

    style();
}

MainWindow::~MainWindow()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    m_window.reset();
    glfwTerminate();
}

bool MainWindow::shouldClose() const noexcept
{
    return glfwWindowShouldClose(m_window.get());
}

void MainWindow::render() noexcept
{
    struct RenderGuard final {
        RenderGuard(GLFWwindow *window) noexcept : window{window}
        {
            glfwPollEvents();

            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

            ImGui::SetNextWindowPos({0, 0});
            int windowSizeX, windowSizeY;
            glfwGetWindowSize(window, &windowSizeX, &windowSizeY);
            ImGui::SetNextWindowSize(
                {static_cast<float>(windowSizeX), static_cast<float>(windowSizeY)});
            ImGui::Begin("No need for a name", nullptr,
                         ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoDecoration |
                             ImGuiWindowFlags_NoBackground);
        }
        RenderGuard(RenderGuard &&) = delete;
        RenderGuard(const RenderGuard &) = delete;
        ~RenderGuard()
        {
            ImGui::End();

            ImGui::Render();
            int display_w, display_h;
            glfwGetFramebufferSize(window, &display_w, &display_h);
            glViewport(0, 0, display_w, display_h);
            glClear(GL_COLOR_BUFFER_BIT);
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            glfwSwapBuffers(window);
        }

        GLFWwindow *window = nullptr;
    };

    auto rg = RenderGuard{m_window.get()};

    renderInputBox();
    renderChannels();
}

bool MainWindow::initializeOpenGlLoader() const noexcept
{
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    return gl3wInit() == 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    return glewInit() == GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    return gladLoadGL();
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD2)
    return gladLoadGL(glfwGetProcAddress);
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING2)
    glbinding::Binding::initialize();
    return true;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING3)
    glbinding::initialize(
        [](const char *name) { return (glbinding::ProcAddress)glfwGetProcAddress(name); });
    return true;
#else
    return true;
#endif
}

void MainWindow::style() noexcept
{
    auto &style = ImGui::GetStyle();
    //    style.Colors[ImGuiCol_Text] = {1.0, 1.0, 1.0, 1.0};
    style.Colors[ImGuiCol_ButtonHovered] = style.Colors[ImGuiCol_Button];
    style.ScaleAllSizes(m_scaleFactor);
}

void MainWindow::renderInputBox() noexcept
{
    auto &style = ImGui::GetStyle();
    ImGui::SetNextWindowPos({style.WindowPadding.x, style.WindowPadding.y});
    const auto windowHeight = ImGui::GetWindowHeight() - (style.WindowPadding.y * 2);
    ImGui::BeginChild(key::windowSelectInput.data(),
                      {key::windowselectinput::width * m_scaleFactor, windowHeight},
                      customimgui::debugMode);

    customimgui::TextUnformattedAligned(key::labelSelectInput.data(), {},
                                        customimgui::Alignment::Center);
    if (customimgui::ToggleButton(key::windowselectinput::buttonLineIn.data(),
                                  m_control.input() == ControlInterface::Input::Line, {-1, 0}))
        m_control.setInput(ControlInterface::Input::Line);
    if (customimgui::ToggleButton(key::windowselectinput::buttonMic.data(),
                                  m_control.input() == ControlInterface::Input::Mic, {-1, 0}))
        m_control.setInput(ControlInterface::Input::Mic);
    if (customimgui::ToggleButton(key::windowselectinput::buttonHiZ.data(),
                                  m_control.input() == ControlInterface::Input::HiZ, {-1, 0}))
        m_control.setInput(ControlInterface::Input::HiZ);
    if (customimgui::ToggleButton(key::windowselectinput::buttonMicHiZ.data(),
                                  m_control.input() == ControlInterface::Input::MicHiZ, {-1, 0}))
        m_control.setInput(ControlInterface::Input::MicHiZ);

    ImGui::Spacing();

    customimgui::TextUnformattedAligned(key::windowselectinput::labelMonitoring.data(), {},
                                        customimgui::Alignment::Center);
    ImGui::PushID(key::windowselectinput::labelMonitoring.data());
    const auto monitoringEnabled = m_control.monitor() == ControlInterface::Monitoring::Enabled;
    if (customimgui::ToggleButton(monitoringEnabled ?
                                      key::windowselectinput::buttonmonitoring::enabled.data() :
                                      key::windowselectinput::buttonmonitoring::disabled.data(),
                                  monitoringEnabled, {-1, 0}))
        m_control.setMonitor(monitoringEnabled ? ControlInterface::Monitoring::Disabled :
                                                 ControlInterface::Monitoring::Enabled);
    ImGui::PopID();

    ImGui::EndChild();
}

void MainWindow::renderChannels() noexcept
{
    auto &style = ImGui::GetStyle();
    const auto windowX =
        (style.WindowPadding.x * 2 + key::windowselectinput::width) * m_scaleFactor;
    ImGui::SetNextWindowPos({windowX, style.WindowPadding.y});
    const auto windowWidth = ImGui::GetWindowWidth() - (style.WindowPadding.x + windowX);
    const auto windowHeight = ImGui::GetWindowHeight() - (style.WindowPadding.y * 2);
    ImGui::BeginChild(key::windowChannels.data(), {windowWidth, windowHeight},
                      customimgui::debugMode);

    const auto channelControlWidth = (windowWidth - (style.WindowPadding.x * 2)) / 2;
    customimgui::ChannelControl(key::windowchannels::input.data(),
                                {channelControlWidth, windowHeight - style.WindowPadding.y * 2},
                                m_control.inputChannel());
    ImGui::SameLine(0, 0);
    customimgui::ChannelControl(key::windowchannels::output.data(),
                                {channelControlWidth, windowHeight - style.WindowPadding.y * 2},
                                m_control.outputChannel());

    ImGui::EndChild();
}

} // namespace ui
} // namespace esimaya22usb
