#pragma once

#include <memory>

#include "../channelinterface.h"
#include "../controlinterface.h"

extern "C" {
struct GLFWwindow;
void glfwDestroyWindow(GLFWwindow *window);
}

namespace esimaya22usb {
namespace ui {

struct Channels final {
    ChannelInterface &input;
    ChannelInterface &output;
};

class MainWindow final {
public:
    MainWindow(ControlInterface &control);
    ~MainWindow();
    [[nodiscard]] bool shouldClose() const noexcept;
    void render() noexcept;

private:
    bool initializeOpenGlLoader() const noexcept;
    void style() noexcept;
    void renderInputBox() noexcept;
    void renderChannels() noexcept;

    ControlInterface &m_control;
    std::unique_ptr<GLFWwindow, decltype(&glfwDestroyWindow)> m_window;
    float m_scaleFactor = 1.0f;
};

} // namespace ui
} // namespace esimaya22usb
