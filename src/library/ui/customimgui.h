#pragma once

#include <iostream>
#include <string_view>

#include <imgui.h>

#include "../channelinterface.h"

namespace esimaya22usb {
namespace ui {
namespace customimgui {

#if defined(NDEBUG)
constexpr auto debugMode = false;
#else
constexpr auto debugMode = false;
#endif // NDEBUG

enum class Alignment { Left, Center, Right };

void TextUnformattedAligned(const std::string_view text, const std::string_view textEnd = {},
                            const Alignment alignment = Alignment::Left)
{
    assert(ImGui::CalcTextSize(text.data()).x <= ImGui::GetWindowWidth());
    switch (alignment) {
        case Alignment::Left:
            break;
        case Alignment::Center:
            ImGui::SetCursorPosX((ImGui::GetWindowWidth() - ImGui::CalcTextSize(text.data()).x) /
                                 2);
            break;
        case Alignment::Right:
            ImGui::SetCursorPosX(ImGui::GetWindowWidth() - ImGui::CalcTextSize(text.data()).x);
            break;
    }
    if (textEnd.empty())
        ImGui::TextUnformatted(text.data());
    else
        ImGui::TextUnformatted(text.data(), textEnd.data());
}

bool ToggleButton(const std::string_view label, const bool toggled, ImVec2 size = {}) noexcept
{
    struct Guard final {
        Guard() noexcept
        {
            ImGui::PushStyleColor(ImGuiCol_Button, ImGui::GetStyle().Colors[ImGuiCol_ButtonActive]);
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered,
                                  ImGui::GetStyle().Colors[ImGuiCol_ButtonActive]);
        }
        Guard(Guard &&) = delete;
        Guard(const Guard &) = delete;
        ~Guard()
        {
            ImGui::PopStyleColor(2);
        }
    };

    if (toggled) {
        Guard guard{};
        return ImGui::Button(label.data(), size);
    }
    return ImGui::Button(label.data(), size);
}

void ChannelControl(const std::string_view label, const ImVec2 size,
                    ChannelInterface &channelData) noexcept
{
    constexpr auto buttonMuteText = "Mute";
    constexpr auto sliderWidth = 10.0f;

    const auto &style = ImGui::GetStyle();
    const auto buttonMuteTextSize = ImGui::CalcTextSize(buttonMuteText);
    const auto buttonMuteSize = ImVec2{buttonMuteTextSize.x + (style.FramePadding.x * 2),
                                       buttonMuteTextSize.y + (style.FramePadding.y * 2)};

    ImGui::BeginChild(label.data(), size, debugMode);

    customimgui::TextUnformattedAligned(label.data(), {}, customimgui::Alignment::Center);

    const auto sliderHeight = size.y -
        (ImGui::GetCursorPosY() + style.ItemSpacing.y + buttonMuteSize.y + style.WindowPadding.y);
    const auto sliderLeftX = (size.x / 4) - (sliderWidth / 2);
    ImGui::SetCursorPosX(sliderLeftX);
    Volume::ValueType volumeLeft = channelData.volumeLeft();
    if (ImGui::VSliderFloat("##left", {sliderWidth, sliderHeight}, &volumeLeft, 0, 1.0f, ""))
        channelData.setVolumeLeft(volumeLeft);
    ImGui::SameLine();
    const auto sliderRightX = (3 * (size.x / 4)) - (sliderWidth / 2);
    ImGui::SetCursorPosX(sliderRightX);
    Volume::ValueType volumeRight = channelData.volumeRight();
    if (ImGui::VSliderFloat("##right", {sliderWidth, sliderHeight}, &volumeRight, 0, 1.0f, ""))
        channelData.setVolumeRight(volumeRight);

    const auto muted = channelData.state() == ChannelInterface::State::Muted;
    ImGui::SetCursorPosX((size.x - buttonMuteSize.x) / 2);
    if (customimgui::ToggleButton(buttonMuteText, muted)) {
        channelData.setState(muted ? ChannelInterface::State::Normal :
                                     ChannelInterface::State::Muted);
        std::clog << "mute" << '\n';
    }

    ImGui::EndChild();
}

} // namespace customimgui
} // namespace ui
} // namespace esimaya22usb
