#pragma once

#include <memory>

#include "channelinterface.h"
#include "controlinterface.h"
#include "hiddeviceinterface.h"
#include "inputchannel.h"
#include "outputchannel.h"

namespace esimaya22usb {

class ChannelFactoryInterface;
class Control final : public ControlInterface {
public:
    explicit Control(std::shared_ptr<HidDeviceInterface> hidDeviceInterface,
                     std::unique_ptr<ChannelFactoryInterface> channelFactory);
    ~Control() override;
    Input input() const noexcept override;
    void setInput(Input) override;
    Monitoring monitor() const noexcept override;
    void setMonitor(Monitoring) override;
    ChannelInterface &inputChannel() const noexcept override;
    ChannelInterface &outputChannel() const noexcept override;

private:
    Input m_input;
    Monitoring m_monitor;
    std::shared_ptr<HidDeviceInterface> m_hidDevice;
    std::unique_ptr<ChannelFactoryInterface> m_channelFactory;
    std::unique_ptr<ChannelInterface> m_inputChannel;
    std::unique_ptr<ChannelInterface> m_outputChannel;
};

} // namespace esimaya22usb
