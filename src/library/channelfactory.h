#pragma once
#include <memory>

#include "channelfactoryinterface.h"
#include "hiddeviceinterface.h"

namespace esimaya22usb {

class ChannelFactory final : public ChannelFactoryInterface {
public:
    explicit ChannelFactory(std::weak_ptr<HidDeviceInterface> hidDevice);
    ~ChannelFactory() override = default;
    std::unique_ptr<ChannelInterface>
        createChannel(ControlInterface::Input) const noexcept override;
    std::unique_ptr<ChannelInterface>
        createChannel(ControlInterface::Output) const noexcept override;

private:
    const std::weak_ptr<HidDeviceInterface> m_hidDevice;
};

} // namespace esimaya22usb
