#pragma once

#include "volume.h"

namespace esimaya22usb {

class ChannelInterface {
public:
    enum class State { Normal = 0, Muted };
    virtual ~ChannelInterface() = default;
    virtual Volume volumeLeft() const noexcept = 0;
    virtual void setVolumeLeft(Volume) = 0;
    virtual Volume volumeRight() const noexcept = 0;
    virtual void setVolumeRight(Volume) = 0;
    virtual State state() const noexcept = 0;
    virtual void setState(State) = 0;
};

} // namespace esimaya22usb
