#pragma once

#include <stdexcept>
#include <string>
#include <vector>

#include <hidapi/hidapi.h>

namespace esimaya22usb {

struct HidException : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

class HidDeviceInfo final {
public:
    explicit HidDeviceInfo(hid_device_info *deviceInfo) noexcept :
        path(deviceInfo->path),
        vendorId(deviceInfo->vendor_id),
        productId(deviceInfo->product_id),
        serialNumber(deviceInfo->serial_number),
        releaseNumber(deviceInfo->release_number),
        manufacturerName(deviceInfo->manufacturer_string),
        productName(deviceInfo->product_string),
        usagePage(deviceInfo->usage_page),
        usage(deviceInfo->usage),
        interfaceNumber(deviceInfo->interface_number)
    {}
    std::string path;
    uint16_t vendorId;
    uint16_t productId;
    std::wstring serialNumber;
    uint16_t releaseNumber;
    std::wstring manufacturerName;
    std::wstring productName;
    uint16_t usagePage;
    uint16_t usage;
    int32_t interfaceNumber;
};

class HidInterface {
public:
    virtual ~HidInterface() = default;
    virtual void init() const = 0;
    virtual void deinit() const = 0;
    virtual std::vector<HidDeviceInfo> availableDevices(uint16_t vendorId,
                                                        uint16_t productId) const = 0;
};

} // namespace esimaya22usb
