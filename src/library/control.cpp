#include "control.h"

#include <cassert>

#include <hidapi/hidapi.h>

#include "channelfactory.h"
#include "globals.h"
#include "hiddevice.h"
#include "i2cwriter.h"

namespace esimaya22usb {

Control::Control(std::shared_ptr<HidDeviceInterface> hidDeviceInterface,
                 std::unique_ptr<ChannelFactoryInterface> channelFactory) :
    m_monitor(Monitoring::Disabled),
    m_hidDevice(hidDeviceInterface),
    m_channelFactory(std::move(channelFactory))
{
    /* enable Headphones on startup */
    I2cWriter i2cWriter(m_hidDevice);
    i2cWriter.write(commands::unmuteHeadphone, 0);

    setInput(Input::Mic);
    m_outputChannel = m_channelFactory->createChannel(Output::Standard);
}

Control::~Control() = default;

ControlInterface::Input Control::input() const noexcept
{
    return m_input;
}

void Control::setInput(ControlInterface::Input input)
{
    m_input = input;
    m_inputChannel = m_channelFactory->createChannel(input);
}

ControlInterface::Monitoring Control::monitor() const noexcept
{
    return m_monitor;
}

void Control::setMonitor(ControlInterface::Monitoring monitor)
{
    I2cWriter i2cWriter(m_hidDevice);
    i2cWriter.write(commands::monitoring, static_cast<uint8_t>(monitor));
    m_monitor = monitor;
}

ChannelInterface &Control::inputChannel() const noexcept
{
    return *m_inputChannel;
}

ChannelInterface &Control::outputChannel() const noexcept
{
    return *m_outputChannel;
}

} // namespace esimaya22usb
