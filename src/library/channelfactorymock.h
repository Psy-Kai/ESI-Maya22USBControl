#pragma once

#include <gmock/gmock.h>

#include "channelfactoryinterface.h"

namespace esimaya22usb {
class ChannelFactoryMock : public ChannelFactoryInterface {
public:
    MOCK_METHOD(std::unique_ptr<ChannelInterface>, createChannel, (ControlInterface::Input),
                (const, noexcept, override));
    MOCK_METHOD(std::unique_ptr<ChannelInterface>, createChannel, (ControlInterface::Output),
                (const, noexcept, override));
};
} // namespace esimaya22usb
