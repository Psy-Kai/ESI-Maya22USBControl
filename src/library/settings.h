#pragma once

#include <nlohmann/json.hpp>

#include "controlinterface.h"

namespace esimaya22usb {

class ChannelInterface;
class Settings {
public:
    explicit Settings();
    void save(const ControlInterface &control);
    void load(ControlInterface &control);

private:
    void saveControl(nlohmann::json &settings, const ControlInterface &control);
    void saveChannel(nlohmann::json &settings, const ChannelInterface &channel);
    void loadControl(const nlohmann::json &settings, ControlInterface &control);
    void loadChannel(const nlohmann::json &settings, ChannelInterface &channel);
};

} // namespace esimaya22usb
