#pragma once

#include <gmock/gmock.h>

#include "channelinterface.h"

namespace esimaya22usb {
class ChannelMock : public ChannelInterface {
    // ChannelInterface interface
public:
    MOCK_METHOD(Volume, volumeLeft, (), (const, noexcept, override));
    MOCK_METHOD(void, setVolumeLeft, (Volume), (override));
    MOCK_METHOD(Volume, volumeRight, (), (const, noexcept, override));
    MOCK_METHOD(void, setVolumeRight, (Volume), (override));
    MOCK_METHOD(State, state, (), (const, noexcept, override));
    MOCK_METHOD(void, setState, (State), (override));
};
} // namespace esimaya22usb
