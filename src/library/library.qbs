import qbs

StaticLibrary {
    name: "ESI-Maya22USB Library"

    cpp.cxxLanguageVersion: "c++17"

    files: [
        "channelfactory.cpp",
        "channelfactory.h",
        "control.cpp",
        "control.h",
        "globals.h",
        "i2cwriter.cpp",
        "i2cwriter.h",
        "inputchannel.cpp",
        "inputchannel.h",
        "outputchannel.cpp",
        "outputchannel.h",
        "settings.cpp",
        "settings.h",
        "ui/customimgui.h",
        "ui/mainwindow.cpp",
        "ui/mainwindow.h",
        "volume.cpp",
        "volume.h",
    ]

    Group {
        name: "HID"
        files: [
            "hid.cpp",
            "hid.h",
            "hiddevice.cpp",
            "hiddevice.h",
        ]
    }

    Group {
        name: "Interfaces"
        files: [
            "channelfactoryinterface.h",
            "channelinterface.h",
            "controlinterface.h",
            "hidinterface.h",
            "hiddeviceinterface.h",
            "i2cwriterinterface.h",
        ]
    }

    Group {
        condition: false
        name: "Mocks"
        files: [
            "channelfactorymock.h",
            "channelmock.h",
            "controlmock.h",
            "hiddevicemock.h",
            "hidmock.h",
            "i2cwritermock.h",
        ]
    }

    Depends {
        name: "hidapi"
    }
    Depends {
        name: "nlohmann_json"
    }
    Depends {
        name: "standard_paths"
    }
    Depends {
        name: "Dear_ImGui"
    }

    Export {
        cpp.includePaths: product.sourceDirectory
        cpp.cxxLanguageVersion: "c++17"

        Depends {
            name: "cpp"
        }
        Depends {
            name: "hidapi"
        }
        Depends {
            name: "nlohmann_json"
        }
        Depends {
            name: "standard_paths"
        }
        Depends {
            name: "Dear_ImGui"
        }
    }
}
