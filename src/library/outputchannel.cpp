#include "outputchannel.h"

#include <cassert>

#include "globals.h"

namespace esimaya22usb {

OutputChannel::OutputChannel(std::unique_ptr<I2cWriterInterface> &&i2cWriter) :
    m_i2cWriter(std::move(i2cWriter)), m_state(State::Normal)
{
    assert(m_i2cWriter != nullptr);
}

Volume OutputChannel::volumeLeft() const noexcept
{
    return m_volumeLeft;
}

void OutputChannel::setVolumeLeft(Volume volume)
{
    m_i2cWriter->write(commands::volumeOutLeft,
                       calcVolume(volume, levelbounds::outMin, levelbounds::outMax));
    m_volumeLeft = volume;
}

Volume OutputChannel::volumeRight() const noexcept
{
    return m_volumeRight;
}

void OutputChannel::setVolumeRight(Volume volume)
{
    m_i2cWriter->write(commands::volumeOutRight,
                       calcVolume(volume, levelbounds::outMin, levelbounds::outMax));
    m_volumeRight = volume;
}

ChannelInterface::State OutputChannel::state() const noexcept
{
    return m_state;
}

void OutputChannel::setState(ChannelInterface::State state)
{
    switch (state) {
        case State::Muted:
            m_i2cWriter->write(commands::volumeOutLeft, levelbounds::outMin);
            m_i2cWriter->write(commands::volumeOutRight, levelbounds::outMin);
            break;
        case State::Normal:
            m_i2cWriter->write(commands::volumeOutLeft,
                               calcVolume(m_volumeLeft, levelbounds::outMin, levelbounds::outMax));
            m_i2cWriter->write(commands::volumeOutRight,
                               calcVolume(m_volumeRight, levelbounds::outMin, levelbounds::outMax));
            break;
    }

    m_state = state;
}

} // namespace esimaya22usb
