#pragma once

#include <memory>

#include "hiddeviceinterface.h"
#include "i2cwriterinterface.h"

namespace esimaya22usb {

class I2cWriter final : public I2cWriterInterface {
public:
    explicit I2cWriter(std::weak_ptr<HidDeviceInterface> hidDevice);
    void write(const uint8_t command, const uint8_t data) override;

private:
    const std::weak_ptr<HidDeviceInterface> m_hidDevice;
};

} // namespace esimaya22usb
