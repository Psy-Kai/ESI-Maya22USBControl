#pragma once

#include <gmock/gmock.h>

#include "hiddeviceinterface.h"

namespace esimaya22usb {
class HidDeviceMock : public HidDeviceInterface {
public:
    MOCK_METHOD(int32_t, write, (const Buffer &data), (override));
    MOCK_METHOD(Buffer, readTimeout, (const std::chrono::milliseconds ms), (const, override));
    MOCK_METHOD(Buffer, read, (), (const, override));
    MOCK_METHOD(int32_t, sendFeatureReport, (const Buffer &featureReport), (override));
    MOCK_METHOD(Buffer, queryFeatureReport, (), (const, override));
    MOCK_METHOD(Text, queryManufacturer, (), (const, override));
    MOCK_METHOD(Text, queryProduct, (), (const, override));
    MOCK_METHOD(Text, querySerialNumber, (), (const, override));
    MOCK_METHOD(Text, errorString, (), (const, noexcept, override));

protected:
    MOCK_METHOD(hid_device *, open,
                (uint16_t vendorId, uint16_t productId, const std::wstring &serialNumber),
                (const, override));
    MOCK_METHOD(hid_device *, open, (const std::string &path), (const, override));
    MOCK_METHOD(hid_device *, open, (const HidDeviceInfo &deviceInfo), (const, override));
    MOCK_METHOD(void, close, (hid_device *), (const, override));
};
} // namespace esimaya22usb
