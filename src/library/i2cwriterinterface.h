#pragma once

#include <cstdint>

namespace esimaya22usb {

class I2cWriterInterface {
public:
    virtual ~I2cWriterInterface() = default;
    virtual void write(const uint8_t command, const uint8_t data) = 0;
};

} // namespace esimaya22usb
