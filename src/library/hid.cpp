#include "hid.h"

#include <hidapi/hidapi.h>

namespace esimaya22usb {

void Hid::init() const
{
    if (hid_init() < 0)
        throw HidException{"hid_init() failed"};
}

void Hid::deinit() const
{
    if (hid_exit() < 0)
        throw HidException{"hid_exit() failed"};
}

std::vector<HidDeviceInfo> Hid::availableDevices(uint16_t vendorId, uint16_t productId) const
{
    std::vector<HidDeviceInfo> result;
    auto *hidInfo = hid_enumerate(vendorId, productId);
    while (hidInfo != nullptr) {
        result.emplace_back(hidInfo);
        hidInfo = hidInfo->next;
    }
    return result;
}

} // namespace esimaya22usb
