#pragma once

#include <chrono>

#include "hidinterface.h"

namespace esimaya22usb {

class HidDeviceInterface {
public:
    using Buffer = std::basic_string<uint8_t>;
    using Text = std::wstring;

    virtual ~HidDeviceInterface() = default;
    virtual int32_t write(const Buffer &data) = 0;
    virtual Buffer readTimeout(const std::chrono::milliseconds ms) const = 0;
    virtual Buffer read() const = 0;
    virtual int32_t sendFeatureReport(const Buffer &featureReport) = 0;
    virtual Buffer queryFeatureReport() const = 0;
    virtual Text queryManufacturer() const = 0;
    virtual Text queryProduct() const = 0;
    virtual Text querySerialNumber() const = 0;
    virtual Text errorString() const noexcept = 0;

protected:
    virtual hid_device *open(uint16_t vendorId, uint16_t productId,
                             const std::wstring &serialNumber = {}) const = 0;
    virtual hid_device *open(const std::string &path) const = 0;
    virtual hid_device *open(const HidDeviceInfo &deviceInfo) const = 0;
    virtual void close(hid_device *) const = 0;
};

} // namespace esimaya22usb
