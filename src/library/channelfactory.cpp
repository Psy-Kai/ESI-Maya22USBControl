#include "channelfactory.h"

#include <utility>

#include "globals.h"
#include "i2cwriter.h"
#include "inputchannel.h"
#include "outputchannel.h"

namespace esimaya22usb {

ChannelFactory::ChannelFactory(std::weak_ptr<HidDeviceInterface> hidDevice) :
    m_hidDevice(std::move(hidDevice))
{}

std::unique_ptr<ChannelInterface>
ChannelFactory::createChannel(ControlInterface::Input input) const noexcept
{
    return std::make_unique<InputChannel>(std::make_unique<I2cWriter>(m_hidDevice), input);
}

std::unique_ptr<ChannelInterface>
    ChannelFactory::createChannel(ControlInterface::Output) const noexcept
{
    return std::make_unique<OutputChannel>(std::make_unique<I2cWriter>(m_hidDevice));
}

} // namespace esimaya22usb
