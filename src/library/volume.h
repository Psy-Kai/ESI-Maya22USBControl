#pragma once

namespace esimaya22usb {

class Volume final {
public:
    using ValueType = float;

    Volume(ValueType level);
    bool operator==(Volume &) const noexcept;
    Volume &operator=(const ValueType level);
    operator ValueType() const;

private:
    ValueType m_volumeLevel = 1.0f;
};

} // namespace esimaya22usb
