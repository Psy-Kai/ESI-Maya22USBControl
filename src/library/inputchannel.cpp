#include "inputchannel.h"

#include <cassert>

#include "globals.h"

namespace esimaya22usb {

InputChannel::InputChannel(std::unique_ptr<I2cWriterInterface> i2cWriter,
                           ControlInterface::Input inputType) :
    m_i2cWriter(std::move(i2cWriter)), m_inputType(inputType), m_state(State::Normal)
{
    assert(m_i2cWriter != nullptr);
    m_i2cWriter->write(commands::selectInput, static_cast<uint8_t>(m_inputType));
}

Volume InputChannel::volumeLeft() const noexcept
{
    return m_volumeLeft;
}

void InputChannel::setVolumeLeft(Volume volume)
{
    m_i2cWriter->write(commands::volumeInLeft,
                       calcVolume(volume, levelbounds::inMin, levelbounds::inMax));
    m_volumeLeft = volume;
}

Volume InputChannel::volumeRight() const noexcept
{
    return m_volumeRight;
}

void InputChannel::setVolumeRight(Volume volume)
{
    m_i2cWriter->write(commands::volumeInRight,
                       calcVolume(volume, levelbounds::inMin, levelbounds::inMax));
    m_volumeRight = volume;
}

ChannelInterface::State InputChannel::state() const noexcept
{
    return m_state;
}

void InputChannel::setState(ChannelInterface::State state)
{
    auto data = static_cast<uint8_t>(m_inputType);
    switch (state) {
        case State::Muted:
            data |= commands::muteMask;
            break;
        default:
            break;
    }

    m_i2cWriter->write(commands::selectInput, data);
    m_state = state;
}

} // namespace esimaya22usb
