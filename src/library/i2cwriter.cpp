#include "i2cwriter.h"

#include <cassert>
#include <stdexcept>
#include <utility>

namespace esimaya22usb {

constexpr auto structureSize = 32;

I2cWriter::I2cWriter(std::weak_ptr<HidDeviceInterface> hidDevice) :
    m_hidDevice(std::move(hidDevice))
{}

void I2cWriter::write(const uint8_t command, const uint8_t data)
{
    const auto &hidDevice = m_hidDevice.lock();
    assert(hidDevice != nullptr);

    HidDeviceInterface::Buffer buffer(structureSize, 0);
    buffer[1] = 0x12;
    buffer[2] = 0x34;
    buffer[3] = command;
    buffer[5] = 1;
    buffer[6] = data;
    buffer[22] = 0x80;
    if (hidDevice->write(buffer) != structureSize)
        throw std::runtime_error{"failed to write full buffer"};
}

} // namespace esimaya22usb
