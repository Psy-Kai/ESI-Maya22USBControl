import qbs
import qbs.FileInfo
import qbs.Probes

Project {
    minimumQbsVersion: "1.16.0"

    readonly property path conanfileDir: sourceDirectory

    references: [
        "src",
        "tests",
        FileInfo.joinPaths(conan.generatedFilesPath, "conanbuildinfo.qbs"),
    ]

    qbsSearchPaths: [
        "lib/qbs"
    ]

    StaticLibrary {
        name: "Dear_ImGui"
        readonly property path _resPath: conan.dependencies["imgui"].res_paths[0]
        readonly property path _bindingsPath: FileInfo.joinPaths(_resPath, "bindings")

        Group {
            name: "bindings"
            prefix: _bindingsPath + '/'
            files: [
                "imgui_impl_glfw.cpp",
                "imgui_impl_glfw.h",
                "imgui_impl_opengl3.cpp",
                "imgui_impl_opengl3.h",
            ]
        }

        Depends {
            name: "cpp"
        }
        Depends {
            name: "glew"
        }
        Depends {
            name: "glfw"
        }
        Depends {
            name: "glu"
        }
        Depends {
            name: "imgui"
        }

        Export {
            cpp.includePaths: product._resPath

            Depends {
                name: "cpp"
            }
            Depends {
                name: "glew"
            }
            Depends {
                name: "glfw"
            }
            Depends {
                name: "glu"
            }
            Depends {
                name: "imgui"
            }
        }
    }

    Probes.ConanfileProbe {
        id: conan
        conanfilePath: FileInfo.joinPaths(project.conanfileDir, "conanfile.py")
        generators: "qbs"
    }
}
